Source: reprozip
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Ghislain Antony Vaillant <ghisvail@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper (>= 10),
               dh-python,
               libsqlite3-dev,
               python3-all-dbg,
               python3-all-dev,
               python3-requests,
               python3-rpaths,
               python3-setuptools,
               python3-usagestats,
               python3-yaml
Standards-Version: 4.0.0
Vcs-Browser: https://anonscm.debian.org/git/debian-science/packages/reprozip.git
Vcs-Git: https://anonscm.debian.org/git/debian-science/packages/reprozip.git
Homepage: https://www.reprozip.org
X-Python3-Version: >= 3.3

Package: reprozip
Architecture: all
Multi-Arch: foreign
Section: utils
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-reprozip
Description: tool for reproducing scientific experiments (packer)
 ReproZip is a tool aimed at simplifying the process of creating
 reproducible experiments from command-line executions, a frequently-used
 common denominator in computational science.
 .
 It tracks operating system calls and creates a package that contains
 all the binaries, files and dependencies required to run a given
 command on the author’s computational environment (packing step). A
 reviewer can then extract the experiment in his environment to
 reproduce the results (unpacking step).
 .
 This package provides the ReproZip packer.

Package: python3-reprozip
Architecture: linux-any
Multi-Arch: same
Section: python
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Description: modules for the ReproZip packer
 ReproZip is a tool aimed at simplifying the process of creating
 reproducible experiments from command-line executions, a frequently-used
 common denominator in computational science.
 .
 It tracks operating system calls and creates a package that contains
 all the binaries, files and dependencies required to run a given
 command on the author’s computational environment (packing step). A
 reviewer can then extract the experiment in his environment to
 reproduce the results (unpacking step).
 .
 This package provides the modules for Python 3.

Package: python3-reprozip-dbg
Architecture: linux-any
Multi-Arch: same
Section: debug
Priority: extra
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
         python3-reprozip (= ${binary:Version})
Description: debug extensions for the ReproZip packer
 ReproZip is a tool aimed at simplifying the process of creating
 reproducible experiments from command-line executions, a frequently-used
 common denominator in computational science.
 .
 It tracks operating system calls and creates a package that contains
 all the binaries, files and dependencies required to run a given
 command on the author’s computational environment (packing step). A
 reviewer can then extract the experiment in his environment to
 reproduce the results (unpacking step).
 .
 This package provides the debug extensions for Python 3.
